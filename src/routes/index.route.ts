import express, { Request, Response } from "express";
import userRouter from "./user.route";
import questionRouter from "./question.route";
import authRouter from "./auth.route";
import quizRouter from "./quizUser.route";
const router = express.Router();

router.get("/", (req: Request, res: Response) => res.json("Welcome to API"));

router.use("/user", userRouter);
router.use("/question", questionRouter);
router.use("/auth", authRouter);
router.use("/quiz", quizRouter);

export default router;
