import express from "express";
import questionController from "../controllers/questionController";
import Middleware from "../middleware";
import Validator from "../validator/index";
const router = express.Router();

router.get(
  "/",
  Middleware.isAuthenticated,
  Middleware.handlerValidationError,
  questionController.getQuestion
);
router.get(
  "/:id",
  Middleware.isAuthenticated,
  Validator.checkIdParam(),
  Middleware.handlerValidationError,
  questionController.getQuestionById
);
router.post(
  "/",
  Middleware.isAuthenticated,
  Validator.checkCreateQuestion(),
  Middleware.handlerValidationError,
  questionController.createQuestion
);
router.patch(
  "/:id",
  Middleware.isAuthenticated,
  Validator.checkIdParam(),
  Validator.checkCreateQuestion(),
  Middleware.handlerValidationError,
  questionController.updateQuestion
);
router.delete(
  "/:id",
  Middleware.isAuthenticated,
  Validator.checkIdParam(),
  Middleware.handlerValidationError,
  questionController.deleteQuestion
);

export default router;
