import express from "express";
import quizUserController from "../controllers/quizUserController";
import Middleware from "../middleware";
const router = express.Router();

router.get(
  "/",
  Middleware.isAuthenticated,
  Middleware.handlerValidationError,
  quizUserController.getQuizUser
);
router.get(
  "/complete",
  Middleware.isAuthenticated,
  Middleware.handlerValidationError,
  quizUserController.getTotalQuizComplete
);
router.post(
  "/",
  Middleware.isAuthenticated,
  Middleware.handlerValidationError,
  quizUserController.doQuizUser
);

export default router;
