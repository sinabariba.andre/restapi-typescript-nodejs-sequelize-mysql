import { body, param, query } from "express-validator";

class Validator {
  checkCreateUser() {
    return [
      body("username")
        .notEmpty()
        .withMessage("This field cannot be null")
        .isLength({ min: 3 })
        .withMessage("username too short"),
      body("password")
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i")
        .withMessage(
          "password must be contain min 8 character, Uppercase, lowercase and number "
        ),
    ];
  }

  checkCreateQuestion() {
    return [
      body("question").notEmpty().withMessage("Insert the question broo"),
      body("a").notEmpty().withMessage("This field cannot be null"),
      body("b").notEmpty().withMessage("This field cannot be null"),
      body("c").notEmpty().withMessage("This field cannot be null"),
      body("d").notEmpty().withMessage("This field cannot be null"),
      body("correct").notEmpty().withMessage("This field cannot be null"),
    ];
  }

  checkPagination() {
    return [
      query("limit")
        .notEmpty()
        .withMessage("The query limit should be not empty")
        .isInt({ min: 1, max: 10 })
        .withMessage("limit value should be number and between 1-10"),
    ];
  }

  checkIdParam() {
    return [
      param("id")
        .notEmpty()
        .withMessage("the value should be not empty")
        .isUUID(4)
        .withMessage("the value should be uuid v4"),
    ];
  }
}
export default new Validator();
