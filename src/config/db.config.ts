import { Sequelize } from "sequelize-typescript";

// config db 
const db = new Sequelize("db_quiz", "root", "", {
  dialect: "mysql",
  logging: false,
});

export default db;
