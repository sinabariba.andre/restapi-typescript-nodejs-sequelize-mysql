import { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import Question from "../models/question";

//  get all question
const getQuestion = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    // check admin
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perPage = 5;
    const total = await Question.count();

    const questions = await Question.findAll({
      where: {},
      offset: (page - 1) * perPage,
    });

    return res.json({
      data: questions,
      page,
      total,
      lastPage: Math.ceil(total / perPage),
    });
  } catch (error) {
    return res.json({ msg: error });
  }
};

// create question
const createQuestion = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    const id = uuidv4();

    // check admin
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const data = await Question.create({ ...req.body, id });
    return res.json({ data, msg: "question has been created" });
  } catch (err) {
    return res.json({
      msg: "fail to create question",
      status: 500,
      route: "/",
    });
  }
};

// get question by id
const getQuestionById = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const { id } = req.params;
    const question = await Question.findByPk(id);
    return res.json({ question });
  } catch (error) {
    res.json({ msg: `question not found` });
  }
};

// update question
const updateQuestion = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const { id } = req.params;
    const data = await Question.findOne({ where: { id } });
    if (!data) {
      res.json({ msg: `fail to update question` });
      return false;
    }

    const updatedata = await data.update({ ...req.body });
    return res.json({
      updatedata,
      msg: `the question has been updated`,
    });
  } catch (err) {
    return res.json({ msg: "fail to updated", status: 500 });
  }
};

// delete question
const deleteQuestion = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "admin") {
      return res.status(401).json({ msg: "restricted " });
    }
    const { id } = req.params;
    const data = await Question.findOne({ where: { id } });
    if (!data) {
      return res.json({ msg: "cannot find the question" });
    }
    const deleteddata = await data.destroy();
    return res.json({ deleteddata, msg: "question has been deleted" });
  } catch (err) {
    return res.json({ msg: "fail to delete question", status: 500 });
  }
};

export default {
  getQuestion,
  getQuestionById,
  createQuestion,
  updateQuestion,
  deleteQuestion,
};
