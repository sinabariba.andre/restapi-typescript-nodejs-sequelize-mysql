import { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import User from "../models/user";
import bcrypt from "bcrypt";

const getUser = async (req: Request, res: Response) => {
  try {
    const user = await User.findAll();
    return res.json(user);
  } catch (error) {
    res.json({ msg: "bad request", status: 500 });
  }
};

const encrypt = (password: string) => {
  return bcrypt.hashSync(password, 10);
};

const createUser = async (req: Request, res: Response) => {
  const id = uuidv4();

  try {
    const user = await User.findOne({ where: { username: req.body.username } });
    if (user) {
      return res.json({ msg: "username unavailable" });
    }
    const data = await User.create({
      ...req.body,
      password: encrypt(req.body.password),
      id,
    });
    return res.json({ data, msg: "user has been created" });
  } catch (err) {
    return res.json({ msg: "fail to create", status: 500 });
  }
};

const getUserById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = await User.findByPk(id);
    if (!data) {
      res.json({ msg: `id ${id} is not found` });
      return false;
    }
    return res.json({ data });
  } catch (error) {
    return res.json({ msg: "fail to create", status: 500, route: "/" });
  }
};

const updateUser = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = await User.findByPk(id);
    if (!data) {
      res.json({ msg: `user is not found` });
      return false;
    }

    const updateQuery = await data.update({
      ...req.body,
      password: data.getDataValue("password"),
    });
    return res.json({
      data: updateQuery,
      msg: `user  has been updated`,
    });
  } catch (err) {
    return res.json({ msg: "fail to create", status: 500, route: "/" });
  }
};

const deleteUser = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = await User.findByPk(id);
    if (!data) {
      res.json({ msg: `user is not found` });
      return false;
    }
    const deleteData = await data.destroy();
    return res.json({ data: deleteData, msg: "user has been deleted" });
  } catch (err) {
    return res.json({ msg: "fail to create", status: 500, route: "/" });
  }
};

export default { getUser, getUserById, createUser, updateUser, deleteUser };
