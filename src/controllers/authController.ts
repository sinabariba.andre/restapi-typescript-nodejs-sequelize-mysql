import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import User from "../models/user";

// login
const loginUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user: any = await User.authenticate({
      username: req.body.username,
      password: req.body.password,
    });
    const token = jwt.sign(
      { userId: user.id, username: user.username, role: user.role },
      "secretKey",
      { expiresIn: "1h" }
    );
    return res.json({ status: "success", msg: "logged in", data: token });
  } catch (error) {
    res.json({ msg: "credential mismatch", status: 401 });
  }
};

export default { loginUser };
