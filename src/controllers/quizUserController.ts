import { Request, Response } from "express";
import { QueryTypes } from "sequelize";
import db from "../config/db.config";
import QuizUser from "../models/quizUser";
import Question from "../models/question";

//  get all quizUser
const getQuizUser = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "user") {
      return res.status(401).json({ msg: "user only " });
    }
    const page: number = parseInt(req.query.page as any) || 1;
    const perPage = 5;
    const total = await Question.count();
    const question = await Question.findAll({
      attributes: ["id", "question", "a", "b", "c", "d"],
      where: {},
      offset: (page - 1) * perPage,
    });
    return res.json({
      username: user.username,
      userId: user.userId,
      data: question,
      page,
      total,
      lastPage: Math.ceil(total / perPage),
    });
  } catch (error) {
    return res.json({ msg: error });
  }
};

// do a quiz
const doQuizUser = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;

    if (user.role != "user") {
      return res.status(401).json({ msg: "user only " });
    }

    const answer = req.body.answer;
    const question_id = req.body.question_id;

    const question = await Question.findOne({
      attributes: ["id", "question", "correct"],
      where: { id: question_id },
    });

    let poin = 0;
    if (question?.get("correct") == answer) {
      poin = 1;
    }

    const saveQuiz = await QuizUser.create({
      answer,
      poin,
      attempt: true,
      question_id,
      user_id: user.userId,
    });
    return res.json({ data: saveQuiz, msg: "good job " });
  } catch (error) {
    return res.json({ msg: error });
  }
};

// get quizUser by id
const getTotalQuizComplete = async (req: Request, res: Response) => {
  try {
    const user = req.body.locals;
    if (user.role != "user") {
      return res.status(401).json({ msg: "user only " });
    }
    const answer = req.body.answer;
    const question_id = req.body.question_id;

    const page: number = parseInt(req.query.page as any) || 1;
    const perPage = 5;
    const result = await db.query(
      `SELECT 
       question ,a, b,c,d, quiz_users.answer as "your answer" , quiz_users.poin 
       FROM 
       questions 
       JOIN quiz_users ON questions.id = quiz_users.question_id
       WHERE quiz_users.user_id = "${user.userId}"
       `,
      {
        type: QueryTypes.SELECT,
      }
    );

    const score = await QuizUser.sum("poin", {
      where: { user_id: user.userId },
    });

    return res.json({ result, score });
  } catch (error) {
    res.json({ msg: `id ${req.params.id} is not found` });
  }
};

export default {
  getQuizUser,
  getTotalQuizComplete,
  doQuizUser,
};
