import { DataTypes, Sequelize, Model } from "sequelize";
import db from "../config/db.config";
import bcrypt from "bcrypt";

class User extends Model {
  password!: string;
  username!: string;

  checkPassword = (password: any) =>
    bcrypt.compareSync(password, this.password);

  static authenticate = async ({ username, password }: any) => {
    try {
      const user = await this.findOne({ where: { username } });
      if (!user) {
        return Promise.reject("user not found");
      }
      const isPasswordValid = user.checkPassword(password);
      if (!isPasswordValid) {
        return Promise.reject("wrong password");
      }

      return Promise.resolve(user);
    } catch (error) {
      return Promise.reject(error);
    }
  };
}

User.init(
  {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "user",
    },
  },
  {
    sequelize: db,
    modelName: "users",
  }
);

export default User;
