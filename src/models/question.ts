import { DataTypes, Model } from "sequelize";
import db from "../config/db.config";

class Question extends Model {}

Question.init(
  {
    question: { type: DataTypes.STRING, allowNull: false },
    a: { type: DataTypes.STRING, allowNull: false },
    b: { type: DataTypes.STRING, allowNull: false },
    c: { type: DataTypes.STRING, allowNull: false },
    d: { type: DataTypes.STRING, allowNull: false },
    correct: { type: DataTypes.STRING, allowNull: false },
    admin_id: { type: DataTypes.STRING, allowNull: false },
  },
  {
    sequelize: db,
    modelName: "questions",
  }
);

export default Question;
