import { DataTypes, Model } from "sequelize";
import db from "../config/db.config";

class QuizUser extends Model {}

QuizUser.init(
  {
    answer: { type: DataTypes.STRING, allowNull: false },
    poin: { type: DataTypes.INTEGER, allowNull: false },
    attempt: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    question_id: { type: DataTypes.STRING, allowNull: false },
    user_id: { type: DataTypes.STRING, allowNull: false },
  },
  {
    sequelize: db,
    modelName: "quiz_users",
  }
);

export default QuizUser;
