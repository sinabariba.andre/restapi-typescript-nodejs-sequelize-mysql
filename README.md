# Instalasi

```
git clone https://gitlab.com/sinabariba.andre/restapi-typescript-nodejs-sequelize-mysql.git
cd restapi-typescript-nodejs-sequelize-mysql
npm install
npm run dev // run development

npm run start 
```
## run db_quiz.sql



# Dokumentasi

## Login
``` 

curl --location --request POST 'http://localhost:5000/api/v1/auth/login' \
--data-raw '{
    "username":"admin1",
    "password":"Qweqwe123"
}'

Response
{
    "status": "success",
    "msg": "logged in",
    "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIyZWQwY2ZkMS1iZWM0LTQ0NGEtOTMwNS1kZDM0OTVlNWJmNGIiLCJ1c2VybmFtZSI6ImFkbWluMSIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTY1MjU2MDMyMSwiZXhwIjoxNjUyNTYzOTIxfQ.RECk5B93zscNVMAcNbHtC3inDl_BdgD1ppvjIBkIeek"
}

``` 

# All documentation
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/c2f059b11e2986248fa4?action=collection%2Fimport)

```
<div class="postman-run-button"
data-postman-action="collection/import"
data-postman-var-1="c2f059b11e2986248fa4"></div>
<script type="text/javascript">
  (function (p,o,s,t,m,a,n) {
    !p[s] && (p[s] = function () { (p[t] || (p[t] = [])).push(arguments); });
    !o.getElementById(s+t) && o.getElementsByTagName("head")[0].appendChild((
      (n = o.createElement("script")),
      (n.id = s+t), (n.async = 1), (n.src = m), n
    ));
  }(window, document, "_pm", "PostmanRunObject", "https://run.pstmn.io/button.js"));
</script>

```