"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
// config db 
const db = new sequelize_typescript_1.Sequelize("db_quiz", "root", "", {
    dialect: "mysql",
    logging: false,
});
exports.default = db;
