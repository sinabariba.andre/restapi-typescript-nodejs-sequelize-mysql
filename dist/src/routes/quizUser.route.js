"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const quizUserController_1 = __importDefault(require("../controllers/quizUserController"));
const middleware_1 = __importDefault(require("../middleware"));
const router = express_1.default.Router();
router.get("/", middleware_1.default.isAuthenticated, middleware_1.default.handlerValidationError, quizUserController_1.default.getQuizUser);
router.get("/complete", middleware_1.default.isAuthenticated, middleware_1.default.handlerValidationError, quizUserController_1.default.getTotalQuizComplete);
router.post("/", middleware_1.default.isAuthenticated, middleware_1.default.handlerValidationError, quizUserController_1.default.doQuizUser);
exports.default = router;
