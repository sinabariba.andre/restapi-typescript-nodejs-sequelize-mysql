"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_route_1 = __importDefault(require("./user.route"));
const question_route_1 = __importDefault(require("./question.route"));
const auth_route_1 = __importDefault(require("./auth.route"));
const quizUser_route_1 = __importDefault(require("./quizUser.route"));
const router = express_1.default.Router();
router.get("/", (req, res) => res.json("Welcome to API"));
router.use("/user", user_route_1.default);
router.use("/question", question_route_1.default);
router.use("/auth", auth_route_1.default);
router.use("/quiz", quizUser_route_1.default);
exports.default = router;
