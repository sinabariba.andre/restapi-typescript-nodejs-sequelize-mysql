"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userController_1 = __importDefault(require("../controllers/userController"));
const middleware_1 = __importDefault(require("../middleware"));
const index_1 = __importDefault(require("../validator/index"));
const router = express_1.default.Router();
router.get('/', middleware_1.default.isAuthenticated, userController_1.default.getUser);
router.get('/:id', middleware_1.default.isAuthenticated, userController_1.default.getUserById);
router.post('/', index_1.default.checkCreateUser(), middleware_1.default.handlerValidationError, userController_1.default.createUser);
router.patch('/:id', middleware_1.default.isAuthenticated, index_1.default.checkIdParam(), index_1.default.checkCreateUser(), middleware_1.default.handlerValidationError, userController_1.default.updateUser);
router.delete('/:id', middleware_1.default.isAuthenticated, userController_1.default.deleteUser);
exports.default = router;
