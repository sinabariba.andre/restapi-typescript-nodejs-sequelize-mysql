"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const questionController_1 = __importDefault(require("../controllers/questionController"));
const middleware_1 = __importDefault(require("../middleware"));
const index_1 = __importDefault(require("../validator/index"));
const router = express_1.default.Router();
router.get("/", middleware_1.default.isAuthenticated, middleware_1.default.handlerValidationError, questionController_1.default.getQuestion);
router.get("/:id", middleware_1.default.isAuthenticated, index_1.default.checkIdParam(), middleware_1.default.handlerValidationError, questionController_1.default.getQuestionById);
router.post("/", middleware_1.default.isAuthenticated, index_1.default.checkCreateQuestion(), middleware_1.default.handlerValidationError, questionController_1.default.createQuestion);
router.patch("/:id", middleware_1.default.isAuthenticated, index_1.default.checkIdParam(), index_1.default.checkCreateQuestion(), middleware_1.default.handlerValidationError, questionController_1.default.updateQuestion);
router.delete("/:id", middleware_1.default.isAuthenticated, index_1.default.checkIdParam(), middleware_1.default.handlerValidationError, questionController_1.default.deleteQuestion);
exports.default = router;
