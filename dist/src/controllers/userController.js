"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
const user_1 = __importDefault(require("../models/user"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const getUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield user_1.default.findAll();
        return res.json(user);
    }
    catch (error) {
        res.json({ msg: "bad request", status: 500 });
    }
});
const encrypt = (password) => {
    return bcrypt_1.default.hashSync(password, 10);
};
const createUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = (0, uuid_1.v4)();
    try {
        const user = yield user_1.default.findOne({ where: { username: req.body.username } });
        if (user) {
            return res.json({ msg: "username unavailable" });
        }
        const data = yield user_1.default.create(Object.assign(Object.assign({}, req.body), { password: encrypt(req.body.password), id }));
        return res.json({ data, msg: "user has been created" });
    }
    catch (err) {
        return res.json({ msg: "fail to create", status: 500 });
    }
});
const getUserById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const data = yield user_1.default.findByPk(id);
        if (!data) {
            res.json({ msg: `id ${id} is not found` });
            return false;
        }
        return res.json({ data });
    }
    catch (error) {
        return res.json({ msg: "fail to create", status: 500, route: "/" });
    }
});
const updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const data = yield user_1.default.findByPk(id);
        if (!data) {
            res.json({ msg: `user is not found` });
            return false;
        }
        const updateQuery = yield data.update(Object.assign(Object.assign({}, req.body), { password: data.getDataValue("password") }));
        return res.json({
            data: updateQuery,
            msg: `user  has been updated`,
        });
    }
    catch (err) {
        return res.json({ msg: "fail to create", status: 500, route: "/" });
    }
});
const deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const data = yield user_1.default.findByPk(id);
        if (!data) {
            res.json({ msg: `user is not found` });
            return false;
        }
        const deleteData = yield data.destroy();
        return res.json({ data: deleteData, msg: "user has been deleted" });
    }
    catch (err) {
        return res.json({ msg: "fail to create", status: 500, route: "/" });
    }
});
exports.default = { getUser, getUserById, createUser, updateUser, deleteUser };
