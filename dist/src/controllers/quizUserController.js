"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
const quizUser_1 = __importDefault(require("../models/quizUser"));
const question_1 = __importDefault(require("../models/question"));
//  get all quizUser
const getQuizUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        if (user.role != "user") {
            return res.status(401).json({ msg: "user only " });
        }
        const page = parseInt(req.query.page) || 1;
        const perPage = 5;
        const total = yield question_1.default.count();
        const question = yield question_1.default.findAll({
            attributes: ["id", "question", "a", "b", "c", "d"],
            where: {},
            offset: (page - 1) * perPage,
        });
        return res.json({
            username: user.username,
            userId: user.userId,
            data: question,
            page,
            total,
            lastPage: Math.ceil(total / perPage),
        });
    }
    catch (error) {
        return res.json({ msg: error });
    }
});
// do a quiz
const doQuizUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        if (user.role != "user") {
            return res.status(401).json({ msg: "user only " });
        }
        const answer = req.body.answer;
        const question_id = req.body.question_id;
        const question = yield question_1.default.findOne({
            attributes: ["id", "question", "correct"],
            where: { id: question_id },
        });
        let poin = 0;
        if ((question === null || question === void 0 ? void 0 : question.get("correct")) == answer) {
            poin = 1;
        }
        const saveQuiz = yield quizUser_1.default.create({
            answer,
            poin,
            attempt: true,
            question_id,
            user_id: user.userId,
        });
        return res.json({ data: saveQuiz, msg: "good job " });
    }
    catch (error) {
        return res.json({ msg: error });
    }
});
// get quizUser by id
const getTotalQuizComplete = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        if (user.role != "user") {
            return res.status(401).json({ msg: "user only " });
        }
        const answer = req.body.answer;
        const question_id = req.body.question_id;
        const page = parseInt(req.query.page) || 1;
        const perPage = 5;
        const result = yield db_config_1.default.query(`SELECT 
       question ,a, b,c,d, quiz_users.answer as "your answer" , quiz_users.poin 
       FROM 
       questions 
       JOIN quiz_users ON questions.id = quiz_users.question_id
       WHERE quiz_users.user_id = "${user.userId}"
       `, {
            type: sequelize_1.QueryTypes.SELECT,
        });
        const score = yield quizUser_1.default.sum("poin", {
            where: { user_id: user.userId },
        });
        return res.json({ result, score });
    }
    catch (error) {
        res.json({ msg: `id ${req.params.id} is not found` });
    }
});
exports.default = {
    getQuizUser,
    getTotalQuizComplete,
    doQuizUser,
};
