"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
const question_1 = __importDefault(require("../models/question"));
//  get all question
const getQuestion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        // check admin
        if (user.role != "admin") {
            return res.status(401).json({ msg: "restricted " });
        }
        const page = parseInt(req.query.page) || 1;
        const perPage = 5;
        const total = yield question_1.default.count();
        const questions = yield question_1.default.findAll({
            where: {},
            offset: (page - 1) * perPage,
        });
        return res.json({
            data: questions,
            page,
            total,
            lastPage: Math.ceil(total / perPage),
        });
    }
    catch (error) {
        return res.json({ msg: error });
    }
});
// create question
const createQuestion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        const id = (0, uuid_1.v4)();
        // check admin
        if (user.role != "admin") {
            return res.status(401).json({ msg: "restricted " });
        }
        const data = yield question_1.default.create(Object.assign(Object.assign({}, req.body), { id }));
        return res.json({ data, msg: "question has been created" });
    }
    catch (err) {
        return res.json({
            msg: "fail to create question",
            status: 500,
            route: "/",
        });
    }
});
// get question by id
const getQuestionById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        if (user.role != "admin") {
            return res.status(401).json({ msg: "restricted " });
        }
        const { id } = req.params;
        const question = yield question_1.default.findByPk(id);
        return res.json({ question });
    }
    catch (error) {
        res.json({ msg: `question not found` });
    }
});
// update question
const updateQuestion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        if (user.role != "admin") {
            return res.status(401).json({ msg: "restricted " });
        }
        const { id } = req.params;
        const data = yield question_1.default.findOne({ where: { id } });
        if (!data) {
            res.json({ msg: `fail to update question` });
            return false;
        }
        const updatedata = yield data.update(Object.assign({}, req.body));
        return res.json({
            updatedata,
            msg: `the question has been updated`,
        });
    }
    catch (err) {
        return res.json({ msg: "fail to updated", status: 500 });
    }
});
// delete question
const deleteQuestion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body.locals;
        if (user.role != "admin") {
            return res.status(401).json({ msg: "restricted " });
        }
        const { id } = req.params;
        const data = yield question_1.default.findOne({ where: { id } });
        if (!data) {
            return res.json({ msg: "cannot find the question" });
        }
        const deleteddata = yield data.destroy();
        return res.json({ deleteddata, msg: "question has been deleted" });
    }
    catch (err) {
        return res.json({ msg: "fail to delete question", status: 500 });
    }
});
exports.default = {
    getQuestion,
    getQuestionById,
    createQuestion,
    updateQuestion,
    deleteQuestion,
};
