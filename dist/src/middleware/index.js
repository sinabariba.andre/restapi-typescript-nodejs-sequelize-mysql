"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class Middleware {
    handlerValidationError(req, res, next) {
        const error = (0, express_validator_1.validationResult)(req);
        if (!error.isEmpty()) {
            return res.json(error.array()[0]);
        }
        next();
    }
    // authentication user
    isAuthenticated(req, res, next) {
        const authHeader = req.get("Authorization");
        if (!authHeader) {
            return res.status(401).json({ mgs: "unauthenticated" });
        }
        const token = authHeader.split(" ")[1];
        // verify jwt
        let decodedToken;
        try {
            decodedToken = jsonwebtoken_1.default.verify(token, "secretKey");
        }
        catch (error) {
            return res.status(401).json({ mgs: "unauthenticated" });
        }
        if (!decodedToken) {
            return res.status(401).json({ mgs: "unauthenticated" });
        }
        req.body.locals = decodedToken;
        next();
    }
}
exports.default = new Middleware();
