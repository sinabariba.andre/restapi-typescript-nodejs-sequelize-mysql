"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
class QuizUser extends sequelize_1.Model {
}
QuizUser.init({
    answer: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    poin: { type: sequelize_1.DataTypes.INTEGER, allowNull: false },
    attempt: { type: sequelize_1.DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    question_id: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    user_id: { type: sequelize_1.DataTypes.STRING, allowNull: false },
}, {
    sequelize: db_config_1.default,
    modelName: "quiz_users",
});
exports.default = QuizUser;
