"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
const bcrypt_1 = __importDefault(require("bcrypt"));
class User extends sequelize_1.Model {
    constructor() {
        super(...arguments);
        this.checkPassword = (password) => bcrypt_1.default.compareSync(password, this.password);
    }
}
_a = User;
User.authenticate = ({ username, password }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield _a.findOne({ where: { username } });
        if (!user) {
            return Promise.reject("user not found");
        }
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) {
            return Promise.reject("wrong password");
        }
        return Promise.resolve(user);
    }
    catch (error) {
        return Promise.reject(error);
    }
});
User.init({
    username: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    password: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    role: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        defaultValue: "user",
    },
}, {
    sequelize: db_config_1.default,
    modelName: "users",
});
exports.default = User;
