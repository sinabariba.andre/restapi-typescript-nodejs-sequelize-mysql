"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
class Question extends sequelize_1.Model {
}
Question.init({
    question: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    a: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    b: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    c: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    d: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    correct: { type: sequelize_1.DataTypes.STRING, allowNull: false },
    admin_id: { type: sequelize_1.DataTypes.STRING, allowNull: false },
}, {
    sequelize: db_config_1.default,
    modelName: "questions",
});
exports.default = Question;
