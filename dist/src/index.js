"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const index_route_1 = __importDefault(require("./routes/index.route"));
const db_config_1 = __importDefault(require("./config/db.config"));
const cors_1 = __importDefault(require("cors"));
const port = 5000;
const app = (0, express_1.default)();
db_config_1.default.sync().then(() => {
    console.log("db connected");
});
app.use(express_1.default.json());
app.use((0, cors_1.default)());
app.use("/api/v1", index_route_1.default);
app.listen(port, () => console.log(`running on port ${port}`));
