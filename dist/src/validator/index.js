"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
class Validator {
    checkCreateUser() {
        return [
            (0, express_validator_1.body)("username")
                .notEmpty()
                .withMessage("This field cannot be null")
                .isLength({ min: 3 })
                .withMessage("username too short"),
            (0, express_validator_1.body)("password")
                .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i")
                .withMessage("password must be contain min 8 character, Uppercase, lowercase and number "),
        ];
    }
    checkCreateQuestion() {
        return [
            (0, express_validator_1.body)("question").notEmpty().withMessage("Insert the question broo"),
            (0, express_validator_1.body)("a").notEmpty().withMessage("This field cannot be null"),
            (0, express_validator_1.body)("b").notEmpty().withMessage("This field cannot be null"),
            (0, express_validator_1.body)("c").notEmpty().withMessage("This field cannot be null"),
            (0, express_validator_1.body)("d").notEmpty().withMessage("This field cannot be null"),
            (0, express_validator_1.body)("correct").notEmpty().withMessage("This field cannot be null"),
        ];
    }
    checkPagination() {
        return [
            (0, express_validator_1.query)("limit")
                .notEmpty()
                .withMessage("The query limit should be not empty")
                .isInt({ min: 1, max: 10 })
                .withMessage("limit value should be number and between 1-10"),
        ];
    }
    checkIdParam() {
        return [
            (0, express_validator_1.param)("id")
                .notEmpty()
                .withMessage("the value should be not empty")
                .isUUID(4)
                .withMessage("the value should be uuid v4"),
        ];
    }
}
exports.default = new Validator();
