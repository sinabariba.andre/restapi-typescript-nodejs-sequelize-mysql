-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2022 at 03:57 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `a` varchar(255) NOT NULL,
  `b` varchar(255) NOT NULL,
  `c` varchar(255) NOT NULL,
  `d` varchar(255) NOT NULL,
  `correct` varchar(255) NOT NULL,
  `admin_id` varchar(255) NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `a`, `b`, `c`, `d`, `correct`, `admin_id`, `createdAt`, `updatedAt`) VALUES
('08f29f1a-8160-4e7f-9d3c-ca81c3da11d2', 'berapa 2 x 1?', '1', '2', '3', '4', 'b', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14'),
('0e53f5a5-98e3-474d-a6fd-d5a74d6ff24e', 'berapa 2 + 1?', '1', '2', '3', '4', 'c', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14'),
('32d555e1-f1da-495d-a095-77651a568c14', 'berapa 2 x 3?', '1', '2', '6', '4', 'c', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14'),
('33cba9b1-8a20-42e2-939d-1796c1af292b', 'berapa 2 x 2?', '1', '2', '3', '4', 'd', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14'),
('7a6a27ad-b33f-4e80-b6ef-b6b0f021e97e', 'berapa 1 + 1?', '1', '2', '3', '4', 'b', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14'),
('bd69923a-fea7-4eaf-87e6-40021f2df147', 'berapa 6 : 6?', '1', '2', '6', '4', 'a', '2ed0cfd1-bec4-444a-9305-dd3495e5bf4b', '2022-05-14', '2022-05-14'),
('c4405e6b-dce7-4d8d-b347-6b7ea9f6c8cb', 'berapa 6 : 1?', '1', '2', '6', '4', 'c', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14'),
('e50aca67-bff6-46ce-8cc7-823753152aed', 'berapa 6 : 3?', '1', '2', '6', '4', 'b', 'd608290c-34a5-4032-8da1-c0671771b485', '2022-05-14', '2022-05-14');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_users`
--

CREATE TABLE `quiz_users` (
  `id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `poin` int(11) NOT NULL,
  `attempt` tinyint(1) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz_users`
--

INSERT INTO `quiz_users` (`id`, `answer`, `poin`, `attempt`, `question_id`, `user_id`, `createdAt`, `updatedAt`) VALUES
(1, 'b', 1, 1, 'd8499fee-5465-4171-9f67-27ef43aa78c7', '11b4e766-0bc2-45f4-83fe-919b2d0a530d', '2022-05-14', '2022-05-14'),
(2, 'b', 1, 1, '08f29f1a-8160-4e7f-9d3c-ca81c3da11d2', 'd8499fee-5465-4171-9f67-27ef43aa78c7', '2022-05-14', '2022-05-14'),
(3, 'b', 1, 1, '7a6a27ad-b33f-4e80-b6ef-b6b0f021e97e', 'd8499fee-5465-4171-9f67-27ef43aa78c7', '2022-05-14', '2022-05-14'),
(4, 'b', 1, 1, '7a6a27ad-b33f-4e80-b6ef-b6b0f021e97e', 'd8499fee-5465-4171-9f67-27ef43aa78c7', '2022-05-14', '2022-05-14'),
(5, 'b', 1, 1, '7a6a27ad-b33f-4e80-b6ef-b6b0f021e97e', 'd8499fee-5465-4171-9f67-27ef43aa78c7', '2022-05-14', '2022-05-14'),
(6, 'b', 1, 1, '7a6a27ad-b33f-4e80-b6ef-b6b0f021e97e', 'd8499fee-5465-4171-9f67-27ef43aa78c7', '2022-05-14', '2022-05-14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
('11b4e766-0bc2-45f4-83fe-919b2d0a530d', 'user1', '$2b$10$5HikDmBbWtNoFbPPn6fzvu19lyJH7SQEFGPWlKV.es510nAHYbw0m', 'user', '2022-05-14', '2022-05-14'),
('2ed0cfd1-bec4-444a-9305-dd3495e5bf4b', 'admin1', '$2b$10$dmPT5YXb7yrkZf3vH2E3Puqz5E8.z.gsqJf9UG3tHFcsrlpjwL/Wy', 'admin', '2022-05-14', '2022-05-14'),
('6c487407-a17b-4b4a-ae2e-e9743ff59d6e', 'user2', '$2b$10$T0KA.esjWwbOmFWmZFlaUuhj.1Y4pmfrQFKaztzYcnXQF9GC0xOpa', 'user', '2022-05-14', '2022-05-14'),
('8389ed33-0834-4279-8c84-7eb88129237a', 'admin2', '$2b$10$2a9h2ArQK.MdcwYnq/012ecfbvE8W92Mwn3ea.279kYyYe2enn88G', 'admin', '2022-05-14', '2022-05-14'),
('b7509610-7291-41be-8537-886c098118f4', 'admin3', '$2b$10$oNzT5tSQzasJ3vMOOwgVNOUvPTaNf2Yfd4spLgrBH66EPhBOiyCZ.', 'admin', '2022-05-14', '2022-05-14'),
('c8ee57f3-7197-4896-8c61-6e71db5b06f0', 'user5', '$2b$10$OY3WSvEZik3KwDGkqvlBCuMeS8tQ0vwi1MHpvOw4RiHv8BJiK0x1i', 'user', '2022-05-14', '2022-05-14'),
('d8499fee-5465-4171-9f67-27ef43aa78c7', 'user3', '$2b$10$lyqpaCnr90LMuWLkzoRPL.2/znURsYKL7tkOSz.407ucwz9Qs6QNu', 'user', '2022-05-14', '2022-05-14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_users`
--
ALTER TABLE `quiz_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quiz_users`
--
ALTER TABLE `quiz_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
